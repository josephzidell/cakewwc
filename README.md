CakeWWC - CakePHP the way we want it to be, adapted by [Websites With Class](http://www.websiteswithclass.com)
=======

[![CakeWWC](http://websiteswithclass.com/img/logo-250a.png)](http://www.websiteswithclass.com)

CakeWWC is our take on CakePHP that we'd like to share with you. Design enhancements, code enhancments, and many small things to make developing CakePHP quicker and easier.

Some Additions / Changes We Made
--------------------------------

* A Custom [Theme](http://book.cakephp.org/2.0/en/views/themes.html) - Controller methods are alphabetized, views have breadcrumbs and better pagination.
* [AssetCompress](https://github.com/markstory/asset_compress) Plugin for CakePHP
* [DebugKit](https://github.com/cakephp/debug_kit) with enhanced CSS inspired by [Pull Request #57](https://github.com/cakephp/debug_kit/pull/57)
* An ```Config/environment.php``` file to handle environments, similar to [Ruby on Rails](http://guides.rubyonrails.org/configuring.html#creating-rails-environments)
* [FontAwesome](http://fortawesome.github.io/Font-Awesome/) The iconic font designed for Bootstrap, used in all branches

Branches
--------

* BootstrapCake - Based on [BootstrapCake Plugin](https://github.com/visionred/BootstrapCake)


CakePHP
=======

[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)

CakePHP is a rapid development framework for PHP which uses commonly known design patterns like Active Record, Association Data Mapping, Front Controller and MVC.
Our primary goal is to provide a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss to flexibility.

Some Handy Links
----------------

[CakePHP](http://www.cakephp.org) - The rapid development PHP framework

[CookBook](http://book.cakephp.org) - THE CakePHP user documentation; start learning here!

[API](http://api.cakephp.org) - A reference to CakePHP's classes

[Plugins](http://plugins.cakephp.org/) - A repository of extensions to the framework

[The Bakery](http://bakery.cakephp.org) - Tips, tutorials and articles

[Community Center](http://community.cakephp.org) - A source for everything community related

[Training](http://training.cakephp.org) - Join a live session and get skilled with the framework

[CakeFest](http://cakefest.org) - Don't miss our annual CakePHP conference

[Cake Software Foundation](http://cakefoundation.org) - Promoting development related to CakePHP

Get Support!
------------

[#cakephp](http://webchat.freenode.net/?channels=#cakephp) on irc.freenode.net - Come chat with us, we have cake

[Google Group](https://groups.google.com/group/cake-php) - Community mailing list and forum

[GitHub Issues](https://github.com/cakephp/cakephp/issues) - Got issues? Please tell us!

[Roadmaps](https://github.com/cakephp/cakephp/wiki#roadmaps) - Want to contribute? Get involved!

[![Bake Status](https://secure.travis-ci.org/cakephp/cakephp.png?branch=master)](http://travis-ci.org/cakephp/cakephp)

![Cake Power](https://raw.github.com/cakephp/cakephp/master/lib/Cake/Console/Templates/skel/webroot/img/cake.power.gif)
