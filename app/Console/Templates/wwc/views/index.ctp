<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php
echo "<?php\n\$this->Html->addCrumb( 'All {$pluralHumanName}' );\n?>\n";
?>
<div class="<?php echo $pluralVar; ?> index">
	<table class="data">
		<thead>
			<tr>
			<?php foreach ($fields as $field): ?>
	<th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
			<?php endforeach; ?>
</tr>
		</thead>
		<tbody>
			<?php
			echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
			echo "\t\t\t<tr>\n";
				$field_linked = false;
				foreach ($fields as $field) {
					$isKey = false;
					if (!empty($associations['belongsTo'])) {
						foreach ($associations['belongsTo'] as $alias => $details) {
							if ($field === $details['foreignKey']) {
								$isKey = true;
								echo "\t\t\t\t<td>\n\t\t\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t\t</td>\n";
								break;
							}
						}
					}
					if ($isKey !== true) {
						echo "\t\t\t\t<td>";
						// no link yet
						if ( !$field_linked ) {
							echo "\n\t\t\t\t\t<?php echo \$this->Html->link(h(\${$singularVar}['{$modelClass}']['{$field}']), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n\t\t\t\t";
							$field_linked = true;
						} else {
							echo "<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>";
						}
						echo "</td>\n";
					}
				}
			echo "\t\t\t</tr>\n";

			echo "\t\t\t<?php endforeach; ?>\n";
			?>
		</tbody>
	</table>

	<p>
		<?php echo "<?php
		echo \$this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>"; ?>
	</p>

	<div class="paging">
		<?php
		echo "<?php\n";
		echo "\t\techo \$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));\n";
		echo "\t\techo \$this->Paginator->numbers(array('separator' => ''));\n";
		echo "\t\techo \$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));\n";
		echo "\t?>\n";
		?>
	</div>
</div>

<div class="actions">
	<ul>
		<li><?php echo "<?php echo \$this->Html->link(__('<i class=\"icon-plus-sign-alt\"></i> New " . $singularHumanName . "'), array('action' => 'add'), array('escape' => false)); ?>"; ?></li>
<?php
		$done = array();
		foreach ($associations as $type => $data) {
			foreach ($data as $alias => $details) {
				if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
					echo "\t\t<li><?php echo \$this->Html->link(__('<i class=\"icon-list\"></i> List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('escape' => false)); ?> </li>\n";
					echo "\t\t<li><?php echo \$this->Html->link(__('<i class=\"icon-plus-sign-alt\"></i> New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false)); ?> </li>\n";
					$done[] = $details['controller'];
				}
			}
		}
		?>
	</ul>
</div>
