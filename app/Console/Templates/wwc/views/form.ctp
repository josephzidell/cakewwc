<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php
echo "<?php\n\$this->Html->addCrumb( 'All {$pluralHumanName}', array( 'action' => 'index' ) );\n";
if (strpos($action, 'add') === false) { // edit
	$singularName = Inflector::variable(Inflector::underscore($modelClass));
	echo "\$this->Html->addCrumb( \"{$singularHumanName} #\${$singularName}Id\", array( 'action' => 'view', \${$singularName}Id ) );\n";
}
echo "\$this->Html->addCrumb( '" . sprintf("%s", Inflector::humanize($action)) . "' );\n";
echo "?>\n";
?>
<div class="<?php echo $pluralVar; ?> form">
<?php echo "\t<?php\n\t\techo \$this->Form->create('{$modelClass}');\n"; ?>
<?php
		echo "\n";
		foreach ($fields as $field) {
			if (strpos($action, 'add') !== false && $field == $primaryKey) {
				continue;
			} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
				echo "\t\techo \$this->Form->input('{$field}');\n";
			}
		}
		if (!empty($associations['hasAndBelongsToMany'])) {
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
				echo "\t\techo \$this->Form->input('{$assocName}');\n";
			}
		}
		echo "\n";
?>
<?php
	echo "\t\techo \$this->Form->end(__('Submit'));\n\t?>\n";
?>
</div>

<div class="actions">
	<ul>
<?php if (strpos($action, 'add') === false): ?>
		<li><?php echo "<?php echo \$this->Form->postLink(__('<i class=\"icon-remove-sign\"></i> Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('escape' => false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>"; ?></li>
<?php endif; ?>
		<li><?php echo "<?php echo \$this->Html->link(__('<i class=\"icon-list\"></i> List " . $pluralHumanName . "'), array('action' => 'index'), array('escape' => false)); ?>"; ?></li>
<?php
		$done = array();
		foreach ($associations as $type => $data) {
			foreach ($data as $alias => $details) {
				if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
					echo "\t\t<li><?php echo \$this->Html->link(__('<i class=\"icon-list\"></i> List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('escape' => false)); ?> </li>\n";
					echo "\t\t<li><?php echo \$this->Html->link(__('<i class=\"icon-plus-sign-alt\"></i> New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false)); ?> </li>\n";
					$done[] = $details['controller'];
				}
			}
		}
?>
	</ul>
</div>
