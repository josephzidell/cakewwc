<ul class="nav nav-pills" id="nav_default">
	<?php
	// debug($controllers);
	foreach ( $controllers as $controllerName => $controller ) :
		if ( array_search('index', $controller['actions']) === false ) {
			continue;
		}
		$linkName = Inflector::tableize($controller['name']);
		$class = $this->request->controller == $linkName ? 'active' : '';
		?>
		<li class="<?php echo $class; ?>"><?php echo $this->Html->link($controller['displayName'], array('controller'=>$linkName, 'action'=>'index')); ?></li>
	<?php endforeach; ?>
</ul>
