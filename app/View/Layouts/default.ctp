<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		// echo $this->Html->meta('icon');

		echo $this->fetch('meta');

		echo $this->AssetCompress->css('styles'); ?>
		<!--[if IE 7]>
		  <?php echo $this->Html->css('font-awesome/font-awesome-ie7.min.css'); ?>
		<![endif]-->
		<?php echo $this->fetch('css'); ?>
</head>
<body>
	<div id="container">
		<div id="header">
			<div class="container">
				<?php echo $this->element( 'nav/default', compact('controllers') ); ?>
				<h1><?php echo $this->Html->link("<i class=\"icon-star\"></i> New Website", '/', array('escape' => false)); ?></h1>
			</div>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php
			$breadcrumbs = $this->Html->getCrumbs( ' &raquo; ' );
			if ( ! empty( $breadcrumbs ) ) {
				echo "<nav id=\"breadcrumbs\">{$breadcrumbs}</nav>";
			}
			?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer"></div>
	</div>
	<?php
	// echo $this->element('sql_dump');
	echo $this->fetch('script');
	?>
	<style>
	@font-face {
	font-family: 'FontAwesome';
	src: url('<?php echo $this->Html->Url( '/font/fontawesome-webfont.eot?v=3.2.1' ); ?>');
	src: url('<?php echo $this->Html->Url( '/font/fontawesome-webfont.eot?#iefix&v=3.2.1' ); ?>') format('embedded-opentype'), url('<?php echo $this->Html->Url( '/font/fontawesome-webfont.woff?v=3.2.1' ); ?>') format('woff'), url('<?php echo $this->Html->Url( '/font/fontawesome-webfont.ttf?v=3.2.1' ); ?>') format('truetype'), url('<?php echo $this->Html->Url( '/font/fontawesome-webfont.svg#fontawesomeregular?v=3.2.1' ); ?>') format('svg');
	font-weight: normal;
	font-style: normal;
	}
	</style>
</body>
</html>
