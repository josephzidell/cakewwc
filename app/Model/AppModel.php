<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public $recursive = -1;
	public $actsAs = array( 'Containable' );

	/**
	 * alias to exists(), just throws exception if not found. Assigns $id to model if passed
	 *
	 * @throws NotFoundException if not found
	 * @param string  $id (optional)
	 * @param mixed   $errorHandler string|callable (optional) a string will be used to render a NotFoundException; an anonymous function will just be called
	 * @return boolean
	 */
	public function check($id = null, $errorHandler = null) {
		if (!empty($id)) {
			$this->id = $id;
		}

		if (!$this->exists()) {
			// execute callback function
			if (is_callable($errorHandler)) {
				$errorHandler();
			// throw exception
			} else {
				if (!is_string($errorHandler)) {
					$errorHandler = __(Inflector::humanize(Inflector::underscore($this->name)) . ' not found');
				}
				throw new NotFoundException($errorHandler);
			}
		}

		return true;
	}

	public function moveUp( $id ) {
		$this->check( $id );
		$record = $this->read();

		if ( $record[$this->alias]['order'] == 1) {
			return false;
		}
		$order = $record[$this->alias]['order'] - 1;

		$neighbors = $this->find( 'neighbors',
			array( 'field' => 'order', 'value' => $record[$this->alias]['order'], 'contain' => array() )
		);

		if ( ! empty( $neighbors['prev'] ) ) {
			$this->id = $neighbors['prev'][$this->alias]['id'];
			$this->saveField( 'order', $record[$this->alias]['order'] );
		}
		$this->id = $id;
		$this->saveField('order', $order);
		return true;
	}

	public function moveDown( $id ) {
		$this->check( $id );
		$record = $this->read();

		if ( $record[$this->alias]['order'] == $this->getHighestOrder() ) {
			return false;
		}
		$order = $record[$this->alias]['order'] + 1;

		$neighbors = $this->find( 'neighbors',
			array( 'field' => 'order', 'value' => $record[$this->alias]['order'], 'contain' => array() )
		);

		if ( ! empty( $neighbors['next'] ) ) {
			$this->id = $neighbors['next'][$this->alias]['id'];
			$this->saveField( 'order', $record[$this->alias]['order'] );
		}
		$this->id = $id;
		$this->saveField('order', $order);
		return true;
	}

	public function getHighestOrder(){
		$highestOrder = $this->find( 'first',array(
			'fields' => array( 'order' ),
			//'conditions' => array( 'Website.is_displayed' => true ),
			'order' => array( 'Order DESC' )
		));
		return isset( $highestOrder[$this->alias]['order'] ) ? $highestOrder[$this->alias]['order'] : 0;
	}


}
