<?php
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 */
// CakePlugin::loadAll(); // Loads all plugins at once
// CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
CakePlugin::load('DebugKit');
